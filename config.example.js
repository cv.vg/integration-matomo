module.exports = {
    /**
     * HTTP server listening port
     * @type {number}
     */
    port: null,
    /**
     * Matomo URL
     * @type {string}
     */
    defaultMatomoUrl: null,
    /**
     * Matomo site ID
     * @type {number}
     */
    defaultMatomoSiteId: null,
    /**
     * Matomo authentication token
     * @type {string}
     */
    defaultMatomoToken: null,
    /**
     * Default webhook secret type
     * @type {('header'|'param')}
     */
    defaultSecretType: null,
    /**
     * Default webhook secret key
     * @type {string}
     */
    defaultSecretKey: null,
    /**
     * Default webhook secret value
     * @type {string}
     */
    defaultSecretValue: null,
    /**
     * Default shorten API base URL
     * @type {string}
     */
    defaultShortenerBaseUrl: null
};