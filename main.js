const
    fastify = require('fastify')(),
    { default: PQueue } = require('p-queue'),
    axios = require('axios'),
    { nanoid } = require('nanoid'),
    { crc64 } = require('crc64-ecma');

const {
    port,
    defaultMatomoUrl,
    defaultMatomoSiteId,
    defaultMatomoToken,
    defaultSecretType,
    defaultSecretKey,
    defaultSecretValue,
    defaultShortenerBaseUrl
} = require('./config.js');

const
    queue = new PQueue({ concurrency: 1 }),
    client = axios.create({ baseURL: defaultMatomoUrl });

client.interceptors.request.use(config => ({
    ...config,
    params: {
        ...config.params,
        'idsite': defaultMatomoSiteId,
        'rec': 1,
        'rand': nanoid(),
        'apiv': 1,
        'token_auth': defaultMatomoToken
    }
}));

fastify.post('/', (
    request,
    reply
) => {
    queue.add(async () => {
        const {
            [{
                header: 'headers',
                param: 'query'
            }[defaultSecretType]]: {
                [defaultSecretKey]: requestSecretValue
            },
            body: {
                id,
                timestamp,
                userAgent,
                ip,
                locale
            }
        } = request;
        if(requestSecretValue !== defaultSecretValue) return;
        await client('matomo.php', {
            method: 'POST',
            params: {
                'action_name': id,
                'url': `${defaultShortenerBaseUrl}/${id}`,
                '_id': crc64([userAgent, ip, locale].filter(Boolean).join('')),
                'ua': userAgent,
                'lang': locale,
                'cip': ip,
                'cdt': timestamp
            }
        });
    }).catch(console.error);
    reply.code(200).send();
});

fastify.listen(port).then(() => console.log(`Server listening to port ${port}`)).catch(console.error);